#!/usr/bin/python
from model import *
from robot import *
import matplotlib.pyplot as plt
import numpy as np
from penalty_contact import *

# inits
inc_t = 0.001
grav = 10.#10.

# objects
mm = Model(inc_t, grav)
rr = Robot(inc_t, grav)
pc = PenaltyContact(rr)

# excitation
Torque_act = np.mat([[0],[0],[0]])

# embedded loop
tt = 0
while tt < 6:

	# read sensors
	Q = rr.Q
	dQ = rr.dQ

	# update model
	mm.update(tt, Q, dQ)

	# quick controller
	kp = 400
	kd = 2 * kp ** 0.5
	Torque_act[0] = -kp * ( rr.Q_act[0] - 0.3 ) - kd * rr.dQ_act[0]
	Torque_act[1] = -kp * ( rr.Q_act[1] + 0.6 ) - kd * rr.dQ_act[1]
	Torque_act[2] = -kp * ( rr.Q_act[2] - 0.3 + 1.57 ) - kd * rr.dQ_act[2]

	rr.update(Torque_act)
	pc.update(tt)

	tt += inc_t

print 'done'

data = np.array( mm.data )
data2 = np.array( mm.data2 )

# plotting
legend_size = 8

plt.clf()

plt.subplot(3,1,1)
plt.plot( data[:,0], data[:,1], label = 'xf' )
plt.plot( data[:,0], data[:,4], label = 'xh' )
plt.xlabel('t[s]')
plt.ylabel('[m]')
plt.title('Positions X')
plt.legend(prop={'size':legend_size})

plt.subplot(3,1,2)
plt.plot( data[:,0], data[:,2], label = 'zf' )
plt.plot( data[:,0], data[:,5], label = 'zh' )
plt.xlabel('t[s]')
plt.ylabel('[m]')
plt.title('Positions Z')
plt.legend(prop={'size':legend_size})

plt.subplot(3,1,3)
plt.plot( data[:,0], data[:,3], label = 'thetaf' )
plt.plot( data[:,0], data[:,6], label = 'thetah' )
plt.xlabel('t[s]')
plt.ylabel('[rad]')
plt.title('Orientations')
plt.legend(prop={'size':legend_size})

# adjusting subplot
wadjust = 0.4
hadjust = 0.6
plt.subplots_adjust(wspace = wadjust)
plt.subplots_adjust(hspace = hadjust)
plt.savefig('figure.png')

#############################

plt.clf()

plt.subplot(3,2,1)
plt.plot( data2[:,0], data2[:,1], label = 'x3' )
plt.plot( data2[:,0], data2[:,4], label = 'x6' )
plt.xlabel('t[s]')
plt.ylabel('[m]')
plt.title('X3 and X6')
plt.legend(prop={'size':legend_size})
plt.grid(True)

plt.subplot(3,2,2)
plt.plot( data2[:,0], data2[:,1]-data2[:,4], label = 'x3-x6' )
plt.xlabel('t[s]')
plt.ylabel('[m]')
plt.title('Distance X')
plt.legend(prop={'size':legend_size})
plt.grid(True)

plt.subplot(3,2,3)
plt.plot( data2[:,0], data2[:,2], label = 'z3' )
plt.plot( data2[:,0], data2[:,5], label = 'z6' )
plt.xlabel('t[s]')
plt.ylabel('[m]')
plt.title('Z3 and Z6')
plt.legend(prop={'size':legend_size})
plt.grid(True)
# plt.ylim(-0.1, 2.1)

plt.subplot(3,2,4)
plt.plot( data2[:,0], data2[:,2]-data2[:,5], label = 'z3-z6' )
plt.xlabel('t[s]')
plt.ylabel('[m]')
plt.title('Distance Z')
plt.legend(prop={'size':legend_size})
plt.grid(True)

plt.subplot(3,2,5)
plt.plot( data2[:,0], data2[:,3], label = 'theta3' )
plt.plot( data2[:,0], data2[:,6], label = 'theta6' )
plt.xlabel('t[s]')
plt.ylabel('[rad]')
plt.title('Orientation3 and Orientation6')
plt.legend(prop={'size':legend_size})
plt.grid(True)
# plt.ylim(-0.1, 2.1)

plt.subplot(3,2,6)
plt.plot( data2[:,0], data2[:,3]-data2[:,6], label = 'theta3-theta6' )
plt.xlabel('t[s]')
plt.ylabel('[rad]')
plt.title('Distance Orientation')
# plt.legend(prop={'size':legend_size})
plt.grid(True)

# adjusting subplot
wadjust = 0.4
hadjust = 0.6
plt.subplots_adjust(wspace = wadjust)
plt.subplots_adjust(hspace = hadjust)
plt.savefig('figure-2.png')

