from robot import *
import math as mth

class PenaltyContact:
	def __init__(self, robot):
		self.rr = robot
		self.penetration_coeff = 2500
		# self.damping_coeff = 2 * self.penetration_coeff**0.5
		self.damping_coeff = 25
		self.friction_coeff = 100
		self.fch = np.zeros((2,1))
		self.fcf = np.zeros((2,1))
		self.data = []


	def update(self, time):
		
		# calculate force
		if self.rr.zh < 0:
			self.fch[1,0] = ( self.penetration_coeff * self.rr.zh 
				+ self.damping_coeff * self.rr.vh[1,0] )
			# self.fch[1,0] = ( self.penetration_coeff * self.rr.zh )
		else:
			self.fch[1,0] = 0

		if self.rr.zf < 0:
			self.fcf[1,0] = ( self.penetration_coeff * self.rr.zf
				+ self.damping_coeff * self.rr.vf[1,0] )
			# self.fcf[1,0] = ( self.penetration_coeff * self.rr.zf )
		else:
			self.fcf[1,0] = 0

		# temp
		# self.fch[1,0] = ( self.penetration_coeff * self.rr.zh 
		# 	+ self.damping_coeff * self.rr.vh[1,0] )
		# self.fch[1,0] = -200
		# self.fcf[1,0] = -200

		self.rr.Torque_contact = self.rr.Jvh.T * self.fch + self.rr.Jvf.T * self.fcf

		# data
		self.data.append([time, self.fch.item(1,0), self.fcf.item(1,0)])		

